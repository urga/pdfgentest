import cairosvg

inputfile = file("LBV_Scorebord_v003.svg", "rb")
outputfilename = "out.pdf"


# TODO:
# in Tree class, of cairosvg/parser.py modue overwrite the self.xml_tree object
# and replace
# myel = tree.xpath('//n:tspan', text="{{ name }}", namespaces={'n': "http://www.w3.org/2000/svg"})
# myel[1].text = 'Dries'

cairosvg.svg2pdf(
    file_obj=inputfile,
    write_to=outputfilename
)
