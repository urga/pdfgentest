import cairocffi as cairo

orig_pdf = file("example.pdf", "w")

surface = cairo.PDFSurface(orig_pdf, 595, 842)
context = cairo.Context(surface)

with context:
    context.move_to(240, 140)
    context.rotate(-0.5)
    context.set_font_size(20)
    context.show_text(u'Hi from cairo!')
context.show_page()

with context:
    context.move_to(240, 140)
    import pdb; pdb.set_trace()
    context.show_text(u'Second string')
# surface.write_to_png('example.pdf')
